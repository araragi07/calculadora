/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

/**
 *
 * @author Estudiantes
 */
public class Calculadora {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // crear una clase operacionaritmetica
        // una clase define un nuevo tipo de dato
        //declarar el objeto
        Operacionaritmetica mioperacion;
        Operaciontrigonometrica miOperacionTrigo;
        Operacionpotenciacion miOperacionpotencia;
        Operacionlogaritmoeuler miOperacioneulerlog;
        //invocar el metodo constructor
        //inicializar la variable
        mioperacion=new Operacionaritmetica();
        miOperacionTrigo=new Operaciontrigonometrica();
        miOperacionpotencia=new Operacionpotenciacion();
        miOperacioneulerlog=new Operacionlogaritmoeuler();
        mioperacion.numero1=50;
        mioperacion.numero2=100;
        
        int resultado=mioperacion.suma();
        int resultado1=mioperacion.restar();
        int resultado2=mioperacion.multiplicar();
        int resultado3=mioperacion.dividir();
        System.out.println(mioperacion.suma());
        System.out.println(mioperacion.restar());
        System.out.println(mioperacion.multiplicar());
        System.out.println(mioperacion.dividir());
        
        miOperacionTrigo.numero=45;
        miOperacionTrigo.numero2=0.5;
        System.out.println(miOperacionTrigo.seno());
        System.out.println(miOperacionTrigo.coseno());
        System.out.println(miOperacionTrigo.tangente());
        System.out.println(miOperacionTrigo.arccoseno());
        System.out.println(miOperacionTrigo.arcseno());
        System.out.println(miOperacionTrigo.arctan());
        
        miOperacionpotencia.numero1=2;
        miOperacionpotencia.numero2=3;
        System.out.println(miOperacionpotencia.elevar());
        System.out.println(miOperacionpotencia.raizcuadrada());
        
        miOperacioneulerlog.faith=6;
        System.out.println(miOperacioneulerlog.exponencial());
        System.out.println(miOperacioneulerlog.logaritmobase10());
        System.out.println(miOperacioneulerlog.logaritmonatural());
        
    }
    
}
